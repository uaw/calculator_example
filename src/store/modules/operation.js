const state = {
  operations: [
  ]
}


const mutations = {
  setOperations(state, operations ) {
    state.operations = operations;
  }
}


const actions = {
  pushOperation({commit, state}, operation) {
    let operations = state.operations
    operations.push(operation);
    commit('setOperations', operations)
  },
}

export default {
  namespaced: true,
  state,
  actions,
  mutations
}