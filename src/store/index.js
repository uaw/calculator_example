import Vue from 'vue'
import Vuex from 'vuex'

import operation from '@/store/modules/operation'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    operation
  },
})